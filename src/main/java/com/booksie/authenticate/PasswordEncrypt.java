package com.booksie.authenticate;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class PasswordEncrypt {
	public static String hash(String password, String algorithm, String saltWord) throws NoSuchAlgorithmException {
		SecureRandom random = new SecureRandom();
		byte[] salt = saltWord.getBytes();
		MessageDigest md = MessageDigest.getInstance(algorithm);
		md.update(salt);
		byte[] hashedPasswordBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < hashedPasswordBytes.length; i++) {
			sb.append(Integer.toString((hashedPasswordBytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		String hashedPassword = sb.toString();
		return new String(hashedPassword);
	}
}
